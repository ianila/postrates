from fastapi import Form
from pydantic import BaseModel

class CODIn(BaseModel):
    weight: int
    amount: float
    
    @classmethod
    def as_form(cls, weight: int = Form(), amount: float = Form()):
        return cls(weight=weight, amount=amount)
