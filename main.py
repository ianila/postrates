__author__ = "Anila Senadheera"
__copyright__ = "Copyright (C) 2022 Anila Senadheera"
__license__ = "GNU GPLv3"
__version__ = "0.2"

from fastapi import FastAPI, HTTPException, status, Depends
from fastapi.middleware.cors import CORSMiddleware

from schemas import CODIn

COD_SERVICE_CHARGE = 50

app = FastAPI(
    title='Post Rates API', 
    version=0.1
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_methods=['*'],
    allow_headers=['*'],
)


@app.get('/', tags=['welcome'])
def index():
    return {'message': 'Welcome to Post Rates App'}


@app.post('/cod', status_code=status.HTTP_200_OK, tags=['cod'])
def cod(cod: CODIn = Depends(CODIn.as_form)):
    commission = 0
    postage = 0

    if cod.amount < 1:
        raise HTTPException(status_code=status.HTTP_406_NOT_ACCEPTABLE, detail='amount must be grater than 0')
    elif cod.amount < 101:
        commission = 2
    elif cod.amount < 201:
        commission = 4
    elif cod.amount < 301:
        commission = 6
    elif cod.amount < 401:
        commission = 8
    elif cod.amount < 501:
        commission = 10
    elif cod.amount < 601:
        commission = 12
    elif cod.amount < 701:
        commission = 14
    elif cod.amount < 801:
        commission = 16
    elif cod.amount < 901:
        commission = 18
    elif cod.amount < 1001:
        commission = 20
    elif cod.amount < 1101:
        commission = 22
    elif cod.amount < 1201:
        commission = 24
    elif cod.amount < 1301:
        commission = 26
    elif cod.amount < 1401:
        commission = 28
    elif cod.amount < 1501:
        commission = 30
    elif cod.amount < 1601:
        commission = 32
    elif cod.amount < 1701:
        commission = 34
    elif cod.amount < 1801:
        commission = 36
    elif cod.amount < 1901:
        commission = 38
    elif cod.amount < 2001:
        commission = 40
    elif cod.amount < 4001:
        commission = 50
    elif cod.amount < 6001:
        commission = 60
    elif cod.amount < 8001:
        commission = 80
    elif cod.amount < 10001:
        commission = 100
    elif cod.amount < 50001:
        commission = 150
    elif cod.amount < 100001:
        commission = 250
    else:
        raise HTTPException(status_code=status.HTTP_406_NOT_ACCEPTABLE, detail='amount must be less than 100001')

    if cod.weight < 1:    
        raise HTTPException(status_code=status.HTTP_406_NOT_ACCEPTABLE, detail='weight must be grater than 0')
    elif cod.weight < 251:
        postage = 200
    elif cod.weight < 501:
        postage = 250
    elif cod.weight < 1001:
        postage = 350
    elif cod.weight < 2001:
        postage = 400
    elif cod.weight < 3001:
        postage = 450
    elif cod.weight < 4001:
        postage = 500
    elif cod.weight < 5001:
        postage = 550
    elif cod.weight < 6001:
        postage = 600
    elif cod.weight < 7001:
        postage = 650
    elif cod.weight < 8001:
        postage = 700
    elif cod.weight < 9001:
        postage = 750
    elif cod.weight < 10001:
        postage = 800
    elif cod.weight < 15001:
        postage = 850
    elif cod.weight < 20001:
        postage = 1100
    elif cod.weight < 25001:
        postage = 1600
    elif cod.weight < 30001:
        postage = 2100
    elif cod.weight < 35001:
        postage = 2600
    elif cod.weight < 40001:
        postage = 3100
    else:
        raise HTTPException(status_code=status.HTTP_406_NOT_ACCEPTABLE, detail='weight must be less than 40001 grams')


    return {
        'commission': commission,
        'postage': postage,
        'service charge': COD_SERVICE_CHARGE,
        'total': commission + postage + COD_SERVICE_CHARGE
        }